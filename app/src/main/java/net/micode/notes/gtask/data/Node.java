/*
 * Copyright (c) 2010-2011, The MiCode Open Source Community (www.micode.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.micode.notes.gtask.data;

import android.database.Cursor;

import org.json.JSONObject;

public abstract class Node {//同步信息的管理结点、用于设置、保存同步动作的信息
    public static final int SYNC_ACTION_NONE = 0;//无需同步，本地和云端信息一致

    public static final int SYNC_ACTION_ADD_REMOTE = 1;//需要在云端添加信息

    public static final int SYNC_ACTION_ADD_LOCAL = 2;//需要在本地添加信息

    public static final int SYNC_ACTION_DEL_REMOTE = 3;//需要在云端删除信息

    public static final int SYNC_ACTION_DEL_LOCAL = 4;//需要在本地删除信息

    public static final int SYNC_ACTION_UPDATE_REMOTE = 5;//需要在云端更新信息

    public static final int SYNC_ACTION_UPDATE_LOCAL = 6;//需要在本地更新信息

    public static final int SYNC_ACTION_UPDATE_CONFLICT = 7;//同步更新出现冲突

    public static final int SYNC_ACTION_ERROR = 8;//同步行为出现错误

    private String mGid;//Node下私有变量

    private String mName;

    private long mLastModified;

    private boolean mDeleted;

    public Node() {//对Node初始化
        mGid = null;
        mName = "";
        mLastModified = 0;
        mDeleted = false;
    }

    public abstract JSONObject getCreateAction(int actionId);

    public abstract JSONObject getUpdateAction(int actionId);

    public abstract void setContentByRemoteJSON(JSONObject js);

    public abstract void setContentByLocalJSON(JSONObject js);

    public abstract JSONObject getLocalJSONFromContent();

    public abstract int getSyncAction(Cursor c);

    public void setGid(String gid) {
        this.mGid = gid;
    }//mGid赋值为gid


    public void setName(String name) {
        this.mName = name;
    }//mName赋值为name

    public void setLastModified(long lastModified) {
        this.mLastModified = lastModified;
    }//mLastModified赋值为lastModified

    public void setDeleted(boolean deleted) {
        this.mDeleted = deleted;
    }//mDeleted赋值为deleted


    public String getGid() {
        return this.mGid;
    }//返回mGid值

    public String getName() {
        return this.mName;
    }//返回mName值

    public long getLastModified() {
        return this.mLastModified;
    }//返回mLastModified值

    public boolean getDeleted() {
        return this.mDeleted;
    }//返回mDeleted值

}
