/*
 * Copyright (c) 2010-2011, The MiCode Open Source Community (www.micode.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.micode.notes.gtask.exception;

/**
 * 小米便签运行过程中行为异常处理
 */
public class ActionFailureException extends RuntimeException {
    /**
     * 序列号，便于在序列化反序列化时进行核验
     */
    private static final long serialVersionUID = 4425249765923293627L;

    /**
     * 默认构造函数，使用super调用父类构造方法
     */
    public ActionFailureException() {
        super();
    }

    /**
     * 带参的构造函数，使用super调用父类构造方法
     * @param paramString   异常信息
     */
    public ActionFailureException(String paramString) {
        super(paramString);
    }

    /**
     * 带参构造函数，指定param和paramThrowable，使用super调用父类构造方法
     * @param paramString   异常信息
     * @param paramThrowable  异常对象
     */
    public ActionFailureException(String paramString, Throwable paramThrowable) {
        super(paramString, paramThrowable);
    }
}
