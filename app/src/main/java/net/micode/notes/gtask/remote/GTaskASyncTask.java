
/*
 * Copyright (c) 2010-2011, The MiCode Open Source Community (www.micode.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.micode.notes.gtask.remote;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import net.micode.notes.R;
import net.micode.notes.ui.NotesListActivity;
import net.micode.notes.ui.NotesPreferenceActivity;

/**
 * 实现GTask的异步操作过程，继承了AsyncTask，并指定泛型
 * showNotification提示当前同步的状态，是一个用于交互的方法
 * doInBackground 在后台线程执行，完成任务的主要工作
 * onProgressUpdate可以使用进度条增加用户体验度 
 * onPostExecute处理UI的方式，在这里面可以使用在doInBackground      
 */ 
public class GTaskASyncTask extends AsyncTask<Void, String, Integer> {

    private static int GTASK_SYNC_NOTIFICATION_ID = 5234235;

    /**
     * 完成接口，规定了onComplete方法，作为回调函数，如果类实现该接口，那么需要实现onComplete方法
     */
    public interface OnCompleteListener {
        void onComplete();
    }

    /**
     * Context类，是Android应用程序的一个抽象类，提供操作Android的4大组件和资源的接口
     */
    private Context mContext;
    /**
     * 管理Notification
     */
    private NotificationManager mNotifiManager;
    /**
     * 管理GTask
     */
    private GTaskManager mTaskManager;
    /**
     * 任务完成的监听器
     */
    private OnCompleteListener mOnCompleteListener;

    /**
     * 带参构造函数
     * @param context 上下文对象
     * @param listener 任务完成的监听器
     */
    public GTaskASyncTask(Context context, OnCompleteListener listener) {
        mContext = context;
        mOnCompleteListener = listener;
        mNotifiManager = (NotificationManager) mContext
                .getSystemService(Context.NOTIFICATION_SERVICE);
        mTaskManager = GTaskManager.getInstance();
    }
    /**
     * 取消同步任务
     */
    public void cancelSync() {
        mTaskManager.cancelSync();
    }

    /**
     *  发布进度消息，系统将会调用onProgressUpdate()方法更新这些值
     * @param message 要发布的进度信息
     */ 
    public void publishProgess(String message) {
        publishProgress(new String[] {
            message
        });
    }

    /**
     * 显示通知
     * @param tickerId 通知的tickerId
     * @param content 通知的内容
     */
    private void showNotification(int tickerId, String content) {
        // 描述想要启动一个Activity、Service、BroadcastBroadcast的意图
        PendingIntent pendingIntent;
        // /如果同步不成功，那么从系统取得一个用于启动一个NotesPreferenceActivity的PendingIntent对象
        if (tickerId != R.string.ticker_success) {

            pendingIntent = PendingIntent.getActivity(mContext, 0, new Intent(mContext,

                    NotesPreferenceActivity.class), 0);

        } else {
            // 如果同步成功，那么从系统取得一个用于启动一个NotesListActivity的PendingIntent对象
            pendingIntent = PendingIntent.getActivity(mContext, 0, new Intent(mContext,

                    NotesListActivity.class), 0);

        }
        // 构造一个Notification的构造器，指定context、是否自动取消、
        Notification.Builder builder = new Notification.Builder(mContext)

                .setAutoCancel(true)

                .setContentTitle(mContext.getString(R.string.app_name))

                .setContentText(content)

                .setContentIntent(pendingIntent)

                .setWhen(System.currentTimeMillis())

                .setOngoing(true);
        // 构造一个Notification实例
        Notification notification=builder.getNotification();
        // 通过notify方法执行一个notification的消息
        mNotifiManager.notify(GTASK_SYNC_NOTIFICATION_ID, notification);

    }

    /**
     * 后台工作
     * @param unused 传递的未使用参数
     * @return 返回同步操作的结果
     */
    @Override
    protected Integer doInBackground(Void... unused) {
        //把 NotesPreferenceActivity.getSyncAccountName(mContext))的字符串内容传进sync_progress_login中
        publishProgess(mContext.getString(R.string.sync_progress_login, NotesPreferenceActivity
                .getSyncAccountName(mContext)));
        // 进行具体的同步操作
        return mTaskManager.sync(mContext, this);
    }

    /**
     * 进程更新时的动作
     * @param progress 包含进度的字符串数组
     */
    @Override
    protected void onProgressUpdate(String... progress) {
        showNotification(R.string.ticker_syncing, progress[0]);
        // 判断是否是GTaskSyncService的实例，如果是则发送广播
        if (mContext instanceof GTaskSyncService) {
            ((GTaskSyncService) mContext).sendBroadcast(progress[0]);
        }
    }

    /**
     * 异步任务执行完后台任务后更新UI界面，展示结果
     * @param result 任务执行结果
     */
    @Override
    protected void onPostExecute(Integer result) {
        if (result == GTaskManager.STATE_SUCCESS) {
            // 如果执行成功
            showNotification(R.string.ticker_success, mContext.getString(
                    R.string.success_sync_account, mTaskManager.getSyncAccount()));
            // 设置最新同步时间
            NotesPreferenceActivity.setLastSyncTime(mContext, System.currentTimeMillis());
        } else if (result == GTaskManager.STATE_NETWORK_ERROR) {
            // 如果发生网络错误
            showNotification(R.string.ticker_fail, mContext.getString(R.string.error_sync_network));
        } else if (result == GTaskManager.STATE_INTERNAL_ERROR) {
            // 如果发生内部错误
            showNotification(R.string.ticker_fail, mContext.getString(R.string.error_sync_internal));
        } else if (result == GTaskManager.STATE_SYNC_CANCELLED) {
            // 如果同步任务取消
            showNotification(R.string.ticker_cancel, mContext
                    .getString(R.string.error_sync_cancelled));
        }
        if (mOnCompleteListener != null) {
            // 如果该对象不为空，新建一个线程执行对应的onComplete方法
            new Thread(new Runnable() {

                public void run() {
                    mOnCompleteListener.onComplete();
                }
            }).start();
        }
    }
}
