/*
 * Copyright (c) 2010-2011, The MiCode Open Source Community (www.micode.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.micode.notes.tool;

import android.content.Context;
import android.database.Cursor;
import android.os.Environment;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;

import net.micode.notes.R;
import net.micode.notes.data.Notes;
import net.micode.notes.data.Notes.DataColumns;
import net.micode.notes.data.Notes.DataConstants;
import net.micode.notes.data.Notes.NoteColumns;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

/**
 * 备份相关工具类，为小米便签的备份提供辅助
 */
public class BackupUtils {
    private static final String TAG = "BackupUtils";
    // Singleton stuff
    private static BackupUtils sInstance;

    /**
     * 静态同步方法，通过synchronized关键字，实现单例模式
     * @param context 上下文对象
     * @return BackupUtils的实例对象
     */
    public static synchronized BackupUtils getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new BackupUtils(context);
        }
        return sInstance;
    }

    /**
     * Following states are signs to represents backup or restore
     * status
     */
    // Currently, the sdcard is not mounted
    // 说明SD卡没有装入手机
    public static final int STATE_SD_CARD_UNMOUONTED           = 0;
    // The backup file not exist
    // 备份文件夹不存在
    public static final int STATE_BACKUP_FILE_NOT_EXIST        = 1;
    // The data is not well formated, may be changed by other programs
    // 数据已被破坏，可能会被修改
    public static final int STATE_DATA_DESTROIED               = 2;
    // Some run-time exception which causes restore or backup fails
    // 发生运行时异常可能会造成恢复或备份失败
    public static final int STATE_SYSTEM_ERROR                 = 3;
    // Backup or restore success
    // 备份或者恢复成功
    public static final int STATE_SUCCESS                      = 4;

    private TextExport mTextExport;

    /**
     * 构造函数
     * @param context 上下文对象
     */
    private BackupUtils(Context context) {
        mTextExport = new TextExport(context);
    }

    /**
     * 静态方法，判断外部存储功能是否可用
     * @return 如果外部存储可用，返回true；否则返回false
     */
    private static boolean externalStorageAvailable() {
        return Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState());
    }

    /**
     * 导出为文本
     * @return 返回导出结果
     */
    public int exportToText() {
        return mTextExport.exportToText();
    }

    /**
     * 获取导出文本文件名
     * @return 导出的文本文件名
     */
    public String getExportedTextFileName() {
        return mTextExport.mFileName;
    }

    /**
     * 获取导出文本文件夹
     * @return 导出文本文件的文件夹
     */
    public String getExportedTextFileDir() {
        return mTextExport.mFileDirectory;
    }

    /**
     * 文本导出类
     */
    private static class TextExport {
        private static final String[] NOTE_PROJECTION = {
                NoteColumns.ID,
                NoteColumns.MODIFIED_DATE,
                NoteColumns.SNIPPET,
                NoteColumns.TYPE
        };

        private static final int NOTE_COLUMN_ID = 0;

        private static final int NOTE_COLUMN_MODIFIED_DATE = 1;

        private static final int NOTE_COLUMN_SNIPPET = 2;

        private static final String[] DATA_PROJECTION = {
                DataColumns.CONTENT,
                DataColumns.MIME_TYPE,
                DataColumns.DATA1,
                DataColumns.DATA2,
                DataColumns.DATA3,
                DataColumns.DATA4,
        };

        private static final int DATA_COLUMN_CONTENT = 0;

        private static final int DATA_COLUMN_MIME_TYPE = 1;

        private static final int DATA_COLUMN_CALL_DATE = 2;

        private static final int DATA_COLUMN_PHONE_NUMBER = 4;

        private final String [] TEXT_FORMAT;
        private static final int FORMAT_FOLDER_NAME          = 0;
        private static final int FORMAT_NOTE_DATE            = 1;
        private static final int FORMAT_NOTE_CONTENT         = 2;

        private Context mContext;
        private String mFileName;
        private String mFileDirectory;

        public TextExport(Context context) {
            TEXT_FORMAT = context.getResources().getStringArray(R.array.format_for_exported_note);
            mContext = context;
            mFileName = "";
            mFileDirectory = "";
        }

        /**
         * 根据id获取对应的格式字符串
         * @param id 格式id
         * @return 对应的格式字符串
         */
        private String getFormat(int id) {
            return TEXT_FORMAT[id];
        }

        /**
         * Export the folder identified by folder id to text
         */
        /**
         * 将文件夹 ID 标识的文件夹导出为文本
         * @param folderId 文件夹ID
         * @param ps 打印流
         */
        private void exportFolderToText(String folderId, PrintStream ps) {
            // Query notes belong to this folder
            // 查询属于这个文件夹的note
            Cursor notesCursor = mContext.getContentResolver().query(Notes.CONTENT_NOTE_URI,
                    NOTE_PROJECTION, NoteColumns.PARENT_ID + "=?", new String[] {
                        folderId
                    }, null);

            if (notesCursor != null) {
                if (notesCursor.moveToFirst()) {
                    do {
                        // Print note's last modified date
                        // 打印最后一次修改时间
                        ps.println(String.format(getFormat(FORMAT_NOTE_DATE), DateFormat.format(
                                mContext.getString(R.string.format_datetime_mdhm),
                                notesCursor.getLong(NOTE_COLUMN_MODIFIED_DATE))));
                        // Query data belong to this note
                        // 查询属于这个便签的数据，将文件导出到text
                        String noteId = notesCursor.getString(NOTE_COLUMN_ID);
                        exportNoteToText(noteId, ps);
                    } while (notesCursor.moveToNext());
                }
                notesCursor.close();
            }
        }
        /**
         * 处理导出的mime类型
         * @param ps 输出流
         * @param dataCursor 数据游标
         * @param mimeType mime类型
         */
        private void handleMimeTypeForExport(PrintStream ps, Cursor dataCursor, String mimeType) {
            if (DataConstants.CALL_NOTE.equals(mimeType)) {
                // Print phone number
                String phoneNumber = dataCursor.getString(DATA_COLUMN_PHONE_NUMBER);
                long callDate = dataCursor.getLong(DATA_COLUMN_CALL_DATE);
                String location = dataCursor.getString(DATA_COLUMN_CONTENT);
                // 如果电话号码不为空
                if (!TextUtils.isEmpty(phoneNumber)) {
                    ps.println(String.format(getFormat(FORMAT_NOTE_CONTENT),
                            phoneNumber));
                }
                // Print call date
                // 打印日期
                ps.println(String.format(getFormat(FORMAT_NOTE_CONTENT), DateFormat
                        .format(mContext.getString(R.string.format_datetime_mdhm),
                                callDate)));
                // Print call attachment location// 打印附件位置
                if (!TextUtils.isEmpty(location)) {
                    ps.println(String.format(getFormat(FORMAT_NOTE_CONTENT),
                            location));
                }
            } else if (DataConstants.NOTE.equals(mimeType)) {
                String content = dataCursor.getString(DATA_COLUMN_CONTENT);
                if (!TextUtils.isEmpty(content)) {
                    ps.println(String.format(getFormat(FORMAT_NOTE_CONTENT),
                            content));
                }
            }
        }
        /**
         * Export note identified by id to a print stream
         */
        /**
         * 将 id 标识的便签导出为文本
         * @param noteId 便签id
         * @param ps 打印流
         */
        private void exportNoteToText(String noteId, PrintStream ps) {
            Cursor dataCursor = mContext.getContentResolver().query(Notes.CONTENT_DATA_URI,
                    DATA_PROJECTION, DataColumns.NOTE_ID + "=?", new String[] {
                        noteId
                    }, null);
            //利用光标来扫描内容，区别为callnote和note两种，靠ps.printline输出
            if (dataCursor != null) {
                if (dataCursor.moveToFirst()) {
                    do {
                        String mimeType = dataCursor.getString(DATA_COLUMN_MIME_TYPE);
                        handleMimeTypeForExport(ps, dataCursor, mimeType);
                    } while (dataCursor.moveToNext());
                }
                dataCursor.close();
            }
            // print a line separator between note
            // 打印一个行分隔符
            try {
                ps.write(new byte[] {
                        Character.LINE_SEPARATOR, Character.LETTER_NUMBER
                });
            } catch (IOException e) {
                Log.e(TAG, e.toString());
            }
        }
        /**
         * 移动文件夹游标并打印文件夹信息
         *
         * @param folderCursor 文件夹游标
         * @param ps 打印流
         */
        private void moveFolderCursor(Cursor folderCursor, PrintStream ps) {
            if (folderCursor.moveToFirst()) {
                do {
                    // Print folder's name
                    // 打印文件夹名称
                    String folderName = "";
                    if(folderCursor.getLong(NOTE_COLUMN_ID) == Notes.ID_CALL_RECORD_FOLDER) {
                        folderName = mContext.getString(R.string.call_record_folder_name);
                    } else {
                        folderName = folderCursor.getString(NOTE_COLUMN_SNIPPET);
                    }
                    if (!TextUtils.isEmpty(folderName)) {
                        ps.println(String.format(getFormat(FORMAT_FOLDER_NAME), folderName));
                    }
                    String folderId = folderCursor.getString(NOTE_COLUMN_ID);
                    exportFolderToText(folderId, ps);
                } while (folderCursor.moveToNext());
            }
        }
        /**
         * 移动便签游标并将内容导出到打印流中
         * @param noteCursor 便签游标
         * @param ps 打印流
         */
        private void moveNoteCursor(Cursor noteCursor, PrintStream ps) {
            if (noteCursor.moveToFirst()) {
                do {
                    ps.println(String.format(getFormat(FORMAT_NOTE_DATE), DateFormat.format(
                            mContext.getString(R.string.format_datetime_mdhm),
                            noteCursor.getLong(NOTE_COLUMN_MODIFIED_DATE))));
                    // Query data belong to this note
                    String noteId = noteCursor.getString(NOTE_COLUMN_ID);
                    exportNoteToText(noteId, ps);
                } while (noteCursor.moveToNext());
            }
        }
        /**
         * Note will be exported as text which is user readable
         */
        /**
         * 便签被导出为用户可读的文本
         * @return 如果导出成功，返回STATE_SUCCESS；如果SD卡未挂载，返回STATE_SD_CARD_UNMOUONTED；如果系统错误，返回STATE_SYSTEM_ERROR
         */
        public int exportToText() {
            if (!externalStorageAvailable()) {
                Log.d(TAG, "Media was not mounted");
                return STATE_SD_CARD_UNMOUONTED;
            }

            PrintStream ps = getExportToTextPrintStream();
            if (ps == null) {
                Log.e(TAG, "get print stream error");
                return STATE_SYSTEM_ERROR;
            }
            // First export folder and its notes
            // 导出文件夹并导出里面包含的便签
            Cursor folderCursor = mContext.getContentResolver().query(
                    Notes.CONTENT_NOTE_URI,
                    NOTE_PROJECTION,
                    "(" + NoteColumns.TYPE + "=" + Notes.TYPE_FOLDER + " AND "
                            + NoteColumns.PARENT_ID + "<>" + Notes.ID_TRASH_FOLER + ") OR "
                            + NoteColumns.ID + "=" + Notes.ID_CALL_RECORD_FOLDER, null, null);

            if (folderCursor != null) {
                moveFolderCursor(folderCursor, ps);
                folderCursor.close();
            }

            // Export notes in root's folder
            // 导出根目录里的便签
            Cursor noteCursor = mContext.getContentResolver().query(
                    Notes.CONTENT_NOTE_URI,
                    NOTE_PROJECTION,
                    NoteColumns.TYPE + "=" + +Notes.TYPE_NOTE + " AND " + NoteColumns.PARENT_ID
                            + "=0", null, null);

            if (noteCursor != null) {
                moveNoteCursor(noteCursor, ps);
                noteCursor.close();
            }
            ps.close();
            return STATE_SUCCESS;
        }

        /**
         * Get a print stream pointed to the file {@generateExportedTextFile}
         */
        /**
         * 获取一个指向文件的打印流
         * @return 返回用于导出到文本的打印流，如果失败则返回null
         */
        private PrintStream getExportToTextPrintStream() {
            File file = generateFileMountedOnSDcard(mContext, R.string.file_path,
                    R.string.file_name_txt_format);
            if (file == null) {
                Log.e(TAG, "create file to exported failed");
                return null;
            }
            mFileName = file.getName();
            mFileDirectory = mContext.getString(R.string.file_path);
            PrintStream ps = null;
            try {
                FileOutputStream fos = new FileOutputStream(file);
                // 将ps输出流指定到特定的文件，导出到文件，而不是直接输出
                ps = new PrintStream(fos);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return null;
            } catch (NullPointerException e) {
                e.printStackTrace();
                return null;
            }
            return ps;
        }
    }

    /**
     * Generate the text file to store imported data
     */
    /**
     * 生成文本文件保存导出的数据
     * @param context 上下文对象
     * @param filePathResId 文件路径资源ID
     * @param fileNameFormatResId 文件名格式资源ID
     * @return 生成的文件对象
     */
    private static File generateFileMountedOnSDcard(Context context, int filePathResId, int fileNameFormatResId) {
        StringBuilder sb = new StringBuilder();
        sb.append(Environment.getExternalStorageDirectory()); // 外部存储路径
        sb.append(context.getString(filePathResId)); // 文件存储路径
        File filedir = new File(sb.toString()); // 存储路径信息
        sb.append(context.getString(
                fileNameFormatResId,
                DateFormat.format(context.getString(R.string.format_date_ymd),
                        System.currentTimeMillis())));
        File file = new File(sb.toString());
        // 如果文件不存在则新建，通过try-catch进行异常处理
        try {
            if (!filedir.exists()) {
                filedir.mkdir();
            }
            if (!file.exists()) {
                file.createNewFile();
            }
            return file;
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}


