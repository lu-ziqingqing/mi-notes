package net.micode.notes.ui;

/**
 * 用来控制密码的视图
 */
public class PasswordView {
    private String strPassword;
    private boolean imgCancel;

    public String getStrPassword() {
        return strPassword;
    }

    public void setStrPassword(String strPassword) {
        this.strPassword = strPassword;
    }

    public boolean isImgCancel() {
        return imgCancel;
    }

    public void setImgCancel(boolean imgCancel) {
        this.imgCancel = imgCancel;
    }
}
