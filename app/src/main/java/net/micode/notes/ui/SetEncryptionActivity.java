package net.micode.notes.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.NumberPicker;
import android.widget.TextView;

import net.micode.notes.R;

/**
 * 输入密码活动界面
 */
public class SetEncryptionActivity extends Activity {
    private ImageButton quit_Icon; // 退出按钮
    private TextView textView; // 文本视图
    private NumberPicker[] numberPickers; // 显示四位数字
    private Button[] buttons; // 输入按钮
    private Button backButton; // 删除按钮
    private int[] passwordDigits = new int[4]; // 四位密码
    private int currentDigitIndex = 0; // 输入到第几位
    private String password; // 密码
    private static int REPEAT_CODE = 101; // 请求号

    /**
     * 创建时初始化资源
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.note_encryption);
        quit_Icon = (ImageButton) findViewById(R.id.iv_quit_icon);
        quit_Icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        numberPickers = new NumberPicker[4];
        numberPickers[0] = (NumberPicker) findViewById(R.id.first);
        numberPickers[1] = (NumberPicker) findViewById(R.id.second);
        numberPickers[2] = (NumberPicker) findViewById(R.id.third);
        numberPickers[3] = (NumberPicker) findViewById(R.id.forth);
        for (NumberPicker np : numberPickers) {
            np.setMinValue(0);
            np.setMaxValue(9);
            np.setValue(0); // 初始值设置为0
        }
        initializeButtonListeners();
    }

    /**
     * 初始化按钮监听器
     */
    private void initializeButtonListeners() {
        buttons = new Button[10];
        buttons[0] = (Button) findViewById(R.id.btn_0);
        buttons[1] = (Button) findViewById(R.id.btn_1);
        buttons[2] = (Button) findViewById(R.id.btn_2);
        buttons[3] = (Button) findViewById(R.id.btn_3);
        buttons[4] = (Button) findViewById(R.id.btn_4);
        buttons[5] = (Button) findViewById(R.id.btn_5);
        buttons[6] = (Button) findViewById(R.id.btn_6);
        buttons[7] = (Button) findViewById(R.id.btn_7);
        buttons[8] = (Button) findViewById(R.id.btn_8);
        buttons[9] = (Button) findViewById(R.id.btn_9);
        // 为每个数字按钮设置点击监听器
        for (int i = 0; i <= 9; i++) {
            buttons[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int digit = Integer.parseInt(((Button) v).getText().toString());
                    if (currentDigitIndex < numberPickers.length) {
                        passwordDigits[currentDigitIndex] = digit;
                        numberPickers[currentDigitIndex].setValue(digit);
                        currentDigitIndex = currentDigitIndex + 1;
                    }
                    if (currentDigitIndex == numberPickers.length) {

                        StringBuilder sb = new StringBuilder();
                        for (int i = 0; i < passwordDigits.length; i++) {
                            sb.append(passwordDigits[i]);
                        }
                        password = sb.toString();
                        Intent intent = new Intent(SetEncryptionActivity.this, RepeatEncryptionActivity.class);
                        startActivityForResult(intent,REPEAT_CODE);
                    }
                }
            });
        }
        backButton = (Button) findViewById(R.id.btn_back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (currentDigitIndex > 0) {
                    numberPickers[currentDigitIndex - 1].setValue(0);
                    currentDigitIndex--;
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REPEAT_CODE) {
            if (resultCode == RESULT_OK) {
                String repeatPassword = data.getStringExtra("password");
                if (repeatPassword.equals(password)) {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("password",password);
                    returnIntent.putExtra("success","true");
                    setResult(RESULT_OK, returnIntent);
                    finish();
                } else {
                    finish();
                }
            } else {
                finish();
            }
        }
    }
}
