package net.micode.notes.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.NumberPicker;
import android.widget.TextView;

import net.micode.notes.R;

/**
 * 取消密码活动界面
 */
public class CancelEncryptionActivity extends Activity {
    private ImageButton quit_Icon; // 退出按键
    private TextView textView; // 文本视图
    private NumberPicker[] numberPickers; // 显示四位数字
    private Button[] buttons; // 按钮
    private Button backButton; // 删除按钮
    private int[] passwordDigits = new int[4]; // 四位密码
    private int currentDigitIndex = 0; // 当前输入到第几位密码

    /**
     * 创建活动时的行为，进行资源的初始化
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.note_cancel_encryption);
        quit_Icon = (ImageButton) findViewById(R.id.iv_quit_icon);
        quit_Icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        numberPickers = new NumberPicker[4];
        numberPickers[0] = (NumberPicker) findViewById(R.id.first);
        numberPickers[1] = (NumberPicker) findViewById(R.id.second);
        numberPickers[2] = (NumberPicker) findViewById(R.id.third);
        numberPickers[3] = (NumberPicker) findViewById(R.id.forth);
        for (NumberPicker np : numberPickers) {
            np.setMinValue(0);
            np.setMaxValue(9);
            np.setValue(0); // 初始值设置为0
        }
        initializeButtonListeners();
    }

    /**
     * 初始化按钮的监听器
     */
    private void initializeButtonListeners() {
        buttons = new Button[10];
        buttons[0] = (Button) findViewById(R.id.btn_0);
        buttons[1] = (Button) findViewById(R.id.btn_1);
        buttons[2] = (Button) findViewById(R.id.btn_2);
        buttons[3] = (Button) findViewById(R.id.btn_3);
        buttons[4] = (Button) findViewById(R.id.btn_4);
        buttons[5] = (Button) findViewById(R.id.btn_5);
        buttons[6] = (Button) findViewById(R.id.btn_6);
        buttons[7] = (Button) findViewById(R.id.btn_7);
        buttons[8] = (Button) findViewById(R.id.btn_8);
        buttons[9] = (Button) findViewById(R.id.btn_9);
        // 为每个数字按钮设置点击监听器
        for (int i = 0; i <= 9; i++) {
            buttons[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int digit = Integer.parseInt(((Button) v).getText().toString());
                    if (currentDigitIndex < numberPickers.length) {
                        passwordDigits[currentDigitIndex] = digit;
                        numberPickers[currentDigitIndex].setValue(digit);
                        currentDigitIndex = currentDigitIndex + 1;
                    }
                    if (currentDigitIndex == numberPickers.length) {
                        Intent returnIntent = new Intent();
                        StringBuilder sb = new StringBuilder();
                        for (int i = 0; i < passwordDigits.length; i++) {
                            sb.append(passwordDigits[i]);
                        }
                        String password = sb.toString();
                        returnIntent.putExtra("password",password);
                        setResult(RESULT_OK, returnIntent);
                        finish();
                    }
                }
            });
        }
        backButton = (Button) findViewById(R.id.btn_back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (currentDigitIndex > 0) {
                    numberPickers[currentDigitIndex - 1].setValue(0);
                    currentDigitIndex--;
                }
            }
        });
    }
}
